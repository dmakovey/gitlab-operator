.if_on_dev: &if_on_dev
  if: '$CI_SERVER_HOST == "dev.gitlab.org" && $CI_PROJECT_PATH == "gitlab/cloud-native/gitlab-operator"'

.if_release_tag: &if_release_tag
  if: '$CI_COMMIT_TAG =~ /^[0-9]+\.[0-9]+\.[0-9]+(-(rc|RC|beta)\d*)?$/'

.if_release_tag_on_dev: &if_release_tag_on_dev
  if: '$CI_SERVER_HOST == "dev.gitlab.org" && $CI_PROJECT_PATH == "gitlab/cloud-native/gitlab-operator" && $CI_COMMIT_TAG =~ /^[0-9]+\.[0-9]+\.[0-9]+(-(rc|RC|beta)\d*)?$/'

.skip_if_release_tag: &skip_if_release_tag
  <<: *if_release_tag
  when: never

.if_default_branch: &if_default_branch
  if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH

.skip_if_default_branch: &skip_if_default_branch
  <<: *if_default_branch
  when: never

.if_default_branch_with_bundle_changes: &if_default_branch_with_bundle_changes
  <<: *if_default_branch
  changes:
  - bundle/**/*

.if_stable_branch: &if_stable_branch
  if: '$CI_COMMIT_REF_NAME =~ /^[0-9]+-[0-9]+-stable$/'

.skip_if_stable_branch: &skip_if_stable_branch
  <<: *if_stable_branch
  when: never

.if_dev_mirror: &if_dev_mirror
  if: '$CI_PROJECT_PATH == "gitlab/cloud-native/gitlab-operator"'

.skip_if_dev_mirror: &skip_if_dev_mirror
  <<: *if_dev_mirror
  when: never

.if_docs_branch: &if_docs_branch
  if: '$CI_COMMIT_REF_NAME =~ /(^docs[\/-].+|.+-docs$)/'

.skip_if_docs_branch: &skip_if_docs_branch
  <<: *if_docs_branch
  when: never

.manual_if_release_tag: &manual_if_release_tag
  <<: *if_release_tag
  when: manual

.manual_if_release_tag_on_dev: &manual_if_release_tag_on_dev
  <<: *if_release_tag_on_dev
  when: manual

.cache:
  variables:
    GOPATH: "${CI_PROJECT_DIR}/.go"
  cache:
    key: ${CI_JOB_NAME}
    paths:
      - .go/pkg/mod/
    when: 'always'

.create_review_template:
  extends: .cache
  stage: review
  script: echo "Creating deployment environment"
  rules:
    - !reference [.skip_if_release_tag]
    - !reference [.skip_if_docs_branch]
    - if: '$CI_COMMIT_BRANCH'

.build_review_template:
  extends: .cache
  stage: review
  variables:
    CLEANUP: "no"
  script:
  - ls -l .build || true
  - ./scripts/test.sh build_kustomized
  artifacts:
    paths:
      #NOTE: this will affect following Jobs providing manifest crafted to be deployed into tests.
      #      where this may backfire is if we push "publish" to happen *after* tests.
      #NOTE2: we need both .build and .install artifacts for later tear-down jobs as well as troubleshooting
      - .build/operator.yaml
      - .build/openshift_resources.yaml
      - .build/glop-${HOSTSUFFIX}.${DOMAIN}.yaml
      - .build/gitlab-${HOSTSUFFIX}.${DOMAIN}.yaml
  rules:
    - !reference [.skip_if_release_tag]
    - if: '$CI_COMMIT_BRANCH'

.review_template:
  extends: .cache
  stage: review
  variables:
    CLEANUP: "no"
  script:
  - ls -l .build || true
  - ls -l .install || true
  - ./scripts/test.sh deploy_kustomized
  - echo "ROOT_PASSWORD=$(kubectl -n $TESTS_NAMESPACE get secret gitlab-gitlab-initial-root-password -ojsonpath='{.data.password}' | base64 --decode ; echo)" >> review.env
  - echo "GITLAB_VERSION=$(kubectl -n $TESTS_NAMESPACE exec -ti $(kubectl -n $TESTS_NAMESPACE get pods | grep sidekiq | awk '{print $1}') cat /srv/gitlab/VERSION)" >> review.env
  artifacts:
    paths:
      #NOTE: this will affect following Jobs providing manifest crafted to be deployed into tests.
      #      where this may backfire is if we push "publish" to happen *after* tests.
      #NOTE2: we need both .build and .install artifacts for later tear-down jobs as well as troubleshooting
      - .install/glop-${HOSTSUFFIX}.${DOMAIN}.yaml
      - .install/gitlab-${HOSTSUFFIX}.${DOMAIN}.yaml
    reports:
      dotenv: review.env
  retry: 1
  rules:
    - !reference [.skip_if_release_tag]
    - !reference [.skip_if_docs_branch]
    - if: '$CI_COMMIT_BRANCH'

.qa:
  image: registry.gitlab.com/gitlab-org/gitlab-omnibus-builder/ruby_docker
  stage: qa
  services:
  - docker:dind
  variables:
    QA_ARTIFACTS_DIR: $CI_PROJECT_DIR
    EE_LICENSE: $GITLAB_QA_EE_LICENSE
    QA_OPTIONS: "--tag smoke"
    KNAPSACK_REPORT_PATH: "knapsack/master_report.json"
    KNAPSACK_TEST_FILE_PATTERN: "qa/specs/features/**/*_spec.rb"
    KNAPSACK_GENERATE_REPORT: "true"
  script:
    - echo $QA_ENVIRONMENT_URL $GITLAB_VERSION
    - gem install gitlab-qa
    - SIGNUP_DISABLED=true QA_DEBUG=true GITLAB_USERNAME=root GITLAB_PASSWORD=$ROOT_PASSWORD GITLAB_ADMIN_USERNAME=root GITLAB_ADMIN_PASSWORD=$ROOT_PASSWORD gitlab-qa Test::Instance::Any EE:$GITLAB_VERSION $QA_ENVIRONMENT_URL -- $QA_OPTIONS
  artifacts:
    when: on_failure
    expire_in: 7d
    paths:
    - ./gitlab-qa-run-*
  allow_failure: true
  parallel: 2
  rules:
    - !reference [.skip_if_docs_branch]
    - !reference [.skip_if_release_tag]
    - if: '$CI_COMMIT_BRANCH'

.stop_review_template:
  stage: cleanup
  needs: [] # if `review` stage fails, still allow this job to be triggered
  variables:
    DEBUG_CLEANUP: "yes"
    CLEANUP: "only"
  script: 
    - ls -l .build || true
    - ls -l .install || true
    - ./scripts/test.sh cleanup
  allow_failure: true
  rules:
    - if: '$CI_COMMIT_BRANCH'
      when: manual
    - !reference [.skip_if_release_tag]
    - !reference [.skip_if_docs_branch]
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      when: manual
    - <<: *if_stable_branch
      when: manual
