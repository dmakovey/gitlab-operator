image: registry.gitlab.com/gitlab-org/gitlab-build-images:gitlab-operator-build-base

variables:
  # Configuration of K8s
  # Namespace within which to run tests
  TESTS_NAMESPACE: "n${CI_COMMIT_SHORT_SHA}"
  DOMAIN_OPENSHIFT_4_6: "apps.ocp-ci-4623.k8s-ft.win"
  DOMAIN_OPENSHIFT_4_7: "apps.ocp-ci-4717.k8s-ft.win"
  DOMAIN_GKE: "gitlab-operator.k8s-ft.win"
  # Namespace built into default manifest
  NAMESPACE: "gitlab-system"
  TAG: ${CI_COMMIT_SHORT_SHA}
  IMG: ${CI_REGISTRY_IMAGE}
  HOSTSUFFIX: "n${CI_COMMIT_SHORT_SHA}"
  TLSSECRETNAME: "gitlab-ci-tls"
  # docker configuration
  DOCKER_DRIVER: overlay2
  DOCKER_HOST: tcp://docker:2375
  # API endpoint: /projects/:id/packages/generic/:package_name/:package_version
  RELEASE_VERSION: "${CI_COMMIT_TAG}"
  K8S_MANIFEST_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/gitlab-operator/${CI_COMMIT_TAG}/gitlab-operator-kubernetes-${CI_COMMIT_TAG}.yaml"
  OCP_MANIFEST_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/gitlab-operator/${CI_COMMIT_TAG}/gitlab-operator-openshift-${CI_COMMIT_TAG}.yaml"
  # OCP_RESOURCES_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/gitlab-operator/${CI_COMMIT_TAG}/openshift-resources-${CI_COMMIT_TAG}.yaml"
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/gitlab-operator/${CI_COMMIT_TAG}"
  SECRET_DETECTION_HISTORIC_SCAN: "true"

stages:
  - prepare
  - build
  - test
  - publish
  - release
  - cluster_tests_approval
  - review
  - qa
  - cleanup
  - report

include:
  - template: Dependency-Scanning.gitlab-ci.yml
    rules:
      - if: '$CI_PROJECT_PATH == "gitlab-org/cloud-native/gitlab-operator"'
  - template: Security/Secret-Detection.gitlab-ci.yml
    rules:
      - if: '$CI_PROJECT_PATH == "gitlab-org/cloud-native/gitlab-operator"'
  - local: .gitlab-ci-templates.yml

default:
  interruptible: true

pull_charts:
  stage: prepare
  script: scripts/retrieve_gitlab_charts.sh
  artifacts:
    paths:
      - charts/
  rules:
    - !reference [.skip_if_docs_branch]
    - !reference [.if_release_tag]
    - if: '$CI_COMMIT_BRANCH'

lint_code:
  extends: .cache
  stage: test
  needs: []
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:golangci-lint-alpine
  script: golangci-lint run --out-format code-climate | tee gl-code-quality-report.json | jq -r '.[] | "\(.location.path):\(.location.lines.begin) \(.description)"'
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
    paths:
      - gl-code-quality-report.json
  rules:
    - !reference [.skip_if_dev_mirror]
    - !reference [.skip_if_release_tag]
    - !reference [.skip_if_docs_branch]
    - if: '$CI_COMMIT_BRANCH'

.test_job:
  extends: .cache
  stage: test
  needs: ["pull_charts"]
  variables:
    HELM_CHARTS: "${CI_PROJECT_DIR}/charts"
    GITLAB_OPERATOR_ASSETS: "${CI_PROJECT_DIR}/hack/assets"
    KUBECONFIG: "" # to ensure that the CI cluster is not used
    USE_EXISTING_CLUSTER: "false" # to ensure we don't use the $KUBECONFIG value
    KUBEBUILDER_ASSETS: "/usr/local/kubebuilder/bin"
  before_script:
    - mkdir coverage
    - export CHART_VERSION=$(sed -n ${VERSION_INDEX}p CHART_VERSIONS)
  rules:
    - !reference [.skip_if_release_tag]
    - !reference [.skip_if_docs_branch]
    - if: '$CI_COMMIT_BRANCH'

unit_tests:
  extends: .test_job
  script: /go/bin/ginkgo -skip 'controller' -cover -outputdir=coverage ./...
  parallel:
    matrix:
      - VERSION_INDEX: ["1", "2", "3"]

slow_unit_tests:
  extends: .test_job
  script: /go/bin/ginkgo -focus 'controller' -cover -outputdir=coverage ./...
  parallel:
    matrix:
      - VERSION_INDEX: ["1", "2", "3"]

.docker_build_job:
  extends: .cache
  stage: release
  needs: ["pull_charts"]
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
    # Update module cache so it can be saved in CI cache (only the dependencies required to build)
    - docker run -v "${GOPATH}:/go" -v "${CI_PROJECT_DIR}:/code" -w /code golang:1.16 go list ./...
  interruptible: false

.podman_build_job:
  extends: .cache
  stage: release
  needs: ["pull_charts"]
  before_script:
    #FIXME buildah should be part of gitlab-operator-builder image
    - apk add --no-cache buildah
    - sed -i 's#^driver.*$#driver = "vfs"#g' /etc/containers/storage.conf
    - podman login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
    # Update module cache so it can be saved in CI cache (only the dependencies required to build)
    - podman run -v "${GOPATH}:/go" -v "${CI_PROJECT_DIR}:/code" -w /code golang:1.16 go list ./...
  interruptible: false

build_branch_image:
  extends: .docker_build_job
  script:
    - docker build -t "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}" -t "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}" .
    - docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}"
    - docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}"
  rules:
    - !reference [.skip_if_docs_branch]
    - !reference [.skip_if_default_branch]
    - !reference [.skip_if_stable_branch]
    - !reference [.skip_if_release_tag]
    - if: '$CI_COMMIT_BRANCH'

build_tag_image:
  extends: .docker_build_job
  script:
    - docker build -t "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}" -t "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}" .
    - docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}"
    - docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}"
  rules:
    - !reference [.if_release_tag]
    # TODO: when dev is part of the official release process,
    # change this to:
    # - !reference [.if_release_tag_on_dev]
  needs:
    - upload_manifest

build_latest_image:
  extends: .docker_build_job
  script:
    - docker build -t "${CI_REGISTRY_IMAGE}:latest" -t "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}" .
    - docker push "${CI_REGISTRY_IMAGE}:latest"
    - docker push "${CI_REGISTRY_IMAGE}:${CI_COMMIT_SHORT_SHA}"
  rules:
    - !reference [.if_default_branch]

build_bundle_image:
  extends: .podman_build_job
  variables:
    BUNDLE_REGISTRY: ${CI_REGISTRY_IMAGE}/bundle
    BUNDLE_IMAGE_TAG: ${CI_COMMIT_SHORT_SHA}
    COMPILE_ONLY: "false"
    DOCKER: "podman"
    OPM_DOCKER: "podman"
    OLM_PACKAGE_NAME: "gitlab-operator-kubernetes"
  script:
    - export OLM_PACKAGE_VERSION=${CI_COMMIT_TAG:-${TAG}}
    - export OPERATOR_TAG=${CI_COMMIT_TAG:-${TAG}}
    - export TAG=${CI_COMMIT_TAG:-${TAG}}
    - scripts/olm_bundle.sh install_operatorsdk install_opm publish
  rules:
    - !reference [.if_release_tag]

approve_cluster_tests:
  stage: cluster_tests_approval
  image: alpine:latest
  script: echo "Proceeding to tests in CI clusters..."
  rules:
    - !reference [.skip_if_docs_branch]
    - !reference [.skip_if_release_tag]
    - !reference [.skip_if_stable_branch]
    - !reference [.skip_if_default_branch]
    - if: '$CI_COMMIT_BRANCH'
      when: manual

build_review_4_6:
  extends: .build_review_template
  variables:
    DOMAIN: $DOMAIN_OPENSHIFT_4_6
    PLATFORM: openshift

create_review_4_6:
  extends: .create_review_template
  environment: &environment_4_6
    name: openshift_4_6/$CI_COMMIT_REF_NAME
    url: https://gitlab-$TESTS_NAMESPACE.$DOMAIN_OPENSHIFT_4_6
    on_stop: stop_review_openshift_4_6
    auto_stop_in: 1 hour
  needs:
    - build_review_4_6

review_4_6:
  extends: .review_template
  variables:
    DOMAIN: $DOMAIN_OPENSHIFT_4_6
    PLATFORM: openshift
  before_script:
    - export KUBECONFIG="$KUBECONFIG_OCP_4_6"
  needs: 
    - create_review_4_6
    # we need build_* dependency to receive appropriate artifacts
    - build_review_4_6
  environment: *environment_4_6
  resource_group: "openshift_4_6/${CI_COMMIT_REF_NAME}"

build_review_4_7:
  extends: .build_review_template
  variables:
    DOMAIN: $DOMAIN_OPENSHIFT_4_7
    PLATFORM: openshift

create_review_4_7:
  extends: .create_review_template
  environment: &environment_4_7
    name: openshift_4_7/$CI_COMMIT_REF_NAME
    url: https://gitlab-$TESTS_NAMESPACE.$DOMAIN_OPENSHIFT_4_7
    on_stop: stop_review_openshift_4_7
    auto_stop_in: 1 hour
  needs:
    - build_review_4_7

review_4_7:
  extends: .review_template
  variables:
    DOMAIN: $DOMAIN_OPENSHIFT_4_7
    PLATFORM: openshift
  before_script:
    - export KUBECONFIG="$KUBECONFIG_OCP_4_7"
  needs: 
    - create_review_4_7
    # we need build_* dependency to receive appropriate artifacts
    - build_review_4_7
  environment: *environment_4_7
  resource_group: "openshift_4_7/${CI_COMMIT_REF_NAME}"

build_review_gke:
  extends: .build_review_template
  variables:
    DOMAIN: $DOMAIN_GKE
    PLATFORM: kubernetes

create_review_gke:
  extends: .create_review_template
  environment: &environment_gke
    name: gke/$CI_COMMIT_REF_NAME
    url: https://gitlab-$TESTS_NAMESPACE.$DOMAIN_GKE
    on_stop: stop_review_gke
    auto_stop_in: 1 hour
  needs:
    - build_review_gke
  
review_gke:
  extends: .review_template
  variables:
    DOMAIN: $DOMAIN_GKE
    PLATFORM: kubernetes
  before_script:
    - export KUBECONFIG="$KUBECONFIG_GKE"
  needs: 
    - create_review_gke
    # we need build_* dependency to receive appropriate artifacts
    - build_review_gke
  environment: *environment_gke
  resource_group: "gke/${CI_COMMIT_REF_NAME}"

qa_4_6:
  extends: .qa
  stage: qa
  variables:
    DOMAIN: $DOMAIN_OPENSHIFT_4_6
    QA_ENVIRONMENT_URL: "https://gitlab-${HOSTSUFFIX}.${DOMAIN}"
  needs:
    - review_4_6

qa_4_7:
  extends: .qa
  stage: qa
  variables:
    DOMAIN: $DOMAIN_OPENSHIFT_4_7
    QA_ENVIRONMENT_URL: "https://gitlab-${HOSTSUFFIX}.${DOMAIN}"
  needs:
    - review_4_7

qa_gke:
  extends: .qa
  stage: qa
  variables:
    DOMAIN: $DOMAIN_GKE
    QA_ENVIRONMENT_URL: "https://gitlab-${HOSTSUFFIX}.${DOMAIN}"
  needs:
    - review_gke

stop_review_openshift_4_6:
  extends: .stop_review_template
  variables:
    DOMAIN: $DOMAIN_OPENSHIFT_4_6
  before_script:
    - export KUBECONFIG="$KUBECONFIG_OCP_4_6"
  environment:
    name: openshift_4_6/$CI_COMMIT_REF_NAME
    action: stop
  needs:
    - build_review_4_6


stop_review_openshift_4_7:
  extends: .stop_review_template
  variables:
    DOMAIN: $DOMAIN_OPENSHIFT_4_7
  before_script:
    - export KUBECONFIG="$KUBECONFIG_OCP_4_7"
  environment:
    name: openshift_4_7/$CI_COMMIT_REF_NAME
    action: stop
  needs:
    - build_review_4_7


stop_review_gke:
  extends: .stop_review_template
  variables:
    DOMAIN: $DOMAIN_GKE
  before_script:
    - export KUBECONFIG="$KUBECONFIG_GKE"
  environment:
    name: gke/$CI_COMMIT_REF_NAME
    action: stop
  needs:
    - build_review_gke

build_manifest:
  stage: build
  script: 
    - export TAG=${CI_COMMIT_TAG:-${TAG}}
    - make build_operator
    - make build_openshift_resources
    - echo "---" > .build/separator
    - cat .build/operator.yaml .build/separator .build/openshift_resources.yaml > .build/operator-openshift.yaml
  artifacts:
    untracked: false
    expire_in: 30 days
    paths:
      - .build/operator.yaml
      - .build/operator-openshift.yaml
      - .build/openshift_resources.yaml
  rules:
    - !reference [.skip_if_docs_branch]
    - if: '$CI_COMMIT_BRANCH'
    - !reference [.if_release_tag]

upload_manifest:
  stage: publish
  image: curlimages/curl:latest
  needs: 
    - build_manifest
  rules:
    - !reference [.if_release_tag_on_dev]
    - !reference [.manual_if_release_tag]
  script:
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
        --upload-file .build/operator.yaml \
        ${K8S_MANIFEST_URL}?status=default
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
        --upload-file .build/operator-openshift.yaml \
        ${OCP_MANIFEST_URL}?status=default
#   - |
#     curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" \
#       --upload-file .build/openshift_resources.yaml \
#       ${OCP_RESOURCES_URL}?status=default

publish_release:
  # Caution, as of 2021-02-02 these assets links require a login, see:
  # https://gitlab.com/gitlab-org/gitlab/-/issues/299384
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - upload_manifest
  rules:
    - !reference [.if_release_tag]
  script: echo "Releasing $CI_COMMIT_TAG from ${K8S_MANIFEST_URL} and ${OCP_MANIFEST_URL}"
  release:
      name: "Release $CI_COMMIT_TAG"
      tag_name: "$CI_COMMIT_TAG"
      description: "$CI_COMMIT_TAG"
      assets:
        links:
          - name: "gitlab-operator-kubernetes.yaml"
            url: "${K8S_MANIFEST_URL}"
          - name: "gitlab-operator-openshift.yaml"
            url: "${OCP_MANIFEST_URL}"
#         - name: "openshift-resources.yaml"
#           url: "${OCP_RESOURCES_URL}"

trigger-public-release:
  stage: release
  image: 'registry.gitlab.com/gitlab-org/gitlab-build-images:alpine-bash-jq-curl-git'
  variables:
    COM_API_OPERATOR_PROJECT_URL: "https://gitlab.com/api/v4/projects/18899486"
  script:
    - pipeline_id=$(curl -fS "${COM_API_OPERATOR_PROJECT_URL}/pipelines?ref=${CI_COMMIT_TAG}" | jq '.[0].id')
    - upload_manifest_job_id=$(curl -fS "${COM_API_OPERATOR_PROJECT_URL}/pipelines/${pipeline_id}/jobs" | jq '.[] | select(.name=="upload_manifest").id')
    - curl -fS --request POST --header "PRIVATE-TOKEN:${COM_OPERATOR_PROJECT_ACCESS_TOKEN}" "${COM_API_OPERATOR_PROJECT_URL}/jobs/${upload_manifest_job_id}/play"
  rules:
    - !reference [.manual_if_release_tag_on_dev]

issue-bot:
  stage: report
  image: registry.gitlab.com/gitlab-org/distribution/issue-bot:latest
  script: /issue-bot
  rules:
    - !reference [.if_release_tag]
    - !reference [.if_stable_branch]
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      when: on_failure
